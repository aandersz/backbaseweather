package com.andersz.weather.network;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public interface NetworkResponseListener {

    void onSuccess(String result);

    void onError();

}
