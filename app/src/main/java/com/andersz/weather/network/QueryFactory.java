package com.andersz.weather.network;

import com.google.android.gms.maps.model.LatLng;

import static com.andersz.weather.C.Chars.AND;
import static com.andersz.weather.C.Ids.APP_ID;
import static com.andersz.weather.C.QueryParams.PARAM_APP_ID;
import static com.andersz.weather.C.QueryParams.PARAM_FORECAST;
import static com.andersz.weather.C.QueryParams.PARAM_LAT;
import static com.andersz.weather.C.QueryParams.PARAM_LON;
import static com.andersz.weather.C.QueryParams.PARAM_UNITS;
import static com.andersz.weather.C.QueryParams.PARAM_WEATHER;
import static com.andersz.weather.C.Url.BASE_URL;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class QueryFactory {

    private String getLatLonQuery (LatLng latLng) {
        return PARAM_LAT + latLng.latitude + AND + PARAM_LON + latLng.longitude;
    }

    private String getAppIdQuery() {
        return PARAM_APP_ID + APP_ID;
    }

    public String getWeatherQuery(LatLng latLng, String units) {
        return BASE_URL + PARAM_WEATHER + getLatLonQuery(latLng) + AND + getAppIdQuery() + AND + PARAM_UNITS + units;
    }

    public String getForecastQuery(LatLng latLng, String units) {
        return BASE_URL + PARAM_FORECAST + getLatLonQuery(latLng) + AND + getAppIdQuery() + AND + PARAM_UNITS + units;
    }

}
