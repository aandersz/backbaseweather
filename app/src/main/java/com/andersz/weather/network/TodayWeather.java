package com.andersz.weather.network;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import static com.andersz.weather.C.Chars.EMPTY;
import static com.andersz.weather.C.Chars.NEW_LINE;
import static com.andersz.weather.C.HTTP.GET;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class TodayWeather extends AsyncTask<String, Integer, String> {
    private NetworkResponseListener listener;

    public TodayWeather(NetworkResponseListener listener) {
        this.listener = listener;
    }

    protected String doInBackground(String... urls) {
        try {

            URL url = new URL(urls[0]);

            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod(GET);

            connection.setDoOutput(true);
            connection.setConnectTimeout(5000);
            connection.setReadTimeout(5000);
            connection.connect();

            BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String content = EMPTY, line;

            while ((line = rd.readLine()) != null) {
                content += line + NEW_LINE;
            }

            return content;

        } catch (IOException e) {
            listener.onError();
            e.printStackTrace();
            return null;
        }

    }

    protected void onProgressUpdate(Integer... progress) {

    }

    protected void onPostExecute(String result) {
        listener.onSuccess(result);
    }

}