package com.andersz.weather.main;

import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.andersz.weather.R;

/**
 * @author Adrian Andersz
 */

public class BaseActivity extends AppCompatActivity {

    /**
     * set fragment to default activity container
     *
     * @param fragment       fragment to insert
     * @param addToBackStack add to backStack if true
     */
    protected void replaceFragment(@NonNull Fragment fragment, boolean addToBackStack) {

        //hide keyboard before open new fragment
        FragmentTransaction transaction = getSupportFragmentManager()
                .beginTransaction();

        transaction.replace(R.id.fragment_container, fragment);

        if (addToBackStack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }


    public void showSnackbar(String message) {
        View view = findViewById(android.R.id.content);
        Snackbar snackbar = Snackbar
                .make(view, message, Snackbar.LENGTH_LONG);

        snackbar.show();
    }
}
