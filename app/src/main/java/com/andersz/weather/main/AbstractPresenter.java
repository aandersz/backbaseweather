package com.andersz.weather.main;

/**
 * Base class for presenters, used in implementing the MVP pattern.
 *
 * @param <V> The view type to associated with this presenter.
 * @author Adrian Andersz
 */
public abstract class AbstractPresenter<V> {

    protected V view;

    /**
     * @param view The view to be controlled by this presenter.
     */
    public final void attachView(final V view) {
        this.view = view;
    }

    /**
     * Clears the view reference held by this presenter.
     */
    public final void detachView() {
        view = null;
    }

    /**
     * @return {@code true} if a non-null view is currently attached.
     */
    public final boolean hasView() {
        return view != null;
    }

    /**
     * Called by the view class, once it's ready to be presented.
     */
    protected abstract void onStart();

    /**
     * Called by the view class, before it's destroyed
     */
    protected void onStop() {
    }

}