package com.andersz.weather;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public interface C {

    interface Chars {
        String AND = "&";
        String EMPTY = "";
        String NEW_LINE = "\n";
    }

    interface ErrorType {
        String GENERIC = "generic";
        String NO_MARKER = "no_marker";
        String NO_NAME = "no_name";
    }

    interface BundleKey {
        String KEY_CITY_ID = "key_city_id";
    }

    interface Url {
        String HELP_SCREEN = "file:///android_asset/help.html";
        String BASE_URL = "http://api.openweathermap.org/data/2.5/";
    }

    interface QueryParams {
        String PARAM_WEATHER = "weather?";
        String PARAM_FORECAST = "forecast?";
        String PARAM_APP_ID = "appid=";
        String PARAM_LAT = "lat=";
        String PARAM_LON = "lon=";
        String PARAM_UNITS = "units=";
    }

    interface Units {
        String METRIC = "metric";
        String IMPERIAL = "imperial";
    }

    interface Ids {
        String APP_ID = "c6e381d8c7ff98f0fee43775817cf6ad";
    }

    interface HTTP {
        String GET = "GET";
    }
}
