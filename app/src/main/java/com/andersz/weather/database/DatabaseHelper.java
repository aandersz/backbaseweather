package com.andersz.weather.database;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.andersz.weather.model.City;
import com.andersz.weather.utils.L;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHelper.class.getSimpleName();

    // Database Info
    private static final String DATABASE_NAME = "weatherDatabase";
    private static final int DATABASE_VERSION = 1;

    // Table Names
    private static final String TABLE_CITIES = "cities";

    // Cities Table Columns
    private static final String KEY_CITY_ID = "id";
    private static final String KEY_LAT = "lat";
    private static final String KEY_LON = "lon";
    private static final String KEY_NAME = "name";

    private static DatabaseHelper INSTANCE;

    public static synchronized DatabaseHelper getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new DatabaseHelper(context.getApplicationContext());
        }
        return INSTANCE;
    }

    private DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.applicationContext = context;
    }

    public DatabaseHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
        this.applicationContext = context.getApplicationContext();
    }

    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);
        db.setForeignKeyConstraintsEnabled(true);
    }

    private static final String SPACE = " ";
    private static final String COMMA = ",";
    private static final String OPEN_BRACKET = "(";
    private static final String CLOSE_BRACKET = ")";


    private static final String CREATE_TABLE = "CREATE TABLE";
    private static final String INTEGER = "INTEGER";
    private static final String AUTOINCREMENT = "AUTOINCREMENT";
    private static final String TEXT = "TEXT";
    private static final String PRIMARY_KEY = "PRIMARY KEY";
    private static final String DROP_TABLE_IF_EXISTS = "DROP TABLE IF EXISTS";

    private Context applicationContext;

    @Override
    public void onCreate(SQLiteDatabase db) {

        //todo - This still looks ugly.
        String CREATE_CITIES_TABLE =
                CREATE_TABLE + SPACE + TABLE_CITIES +
                OPEN_BRACKET +
                KEY_CITY_ID + SPACE + INTEGER + SPACE + PRIMARY_KEY + COMMA +
                KEY_LAT + SPACE + TEXT + COMMA +
                KEY_LON + SPACE + TEXT + COMMA +
                KEY_NAME + SPACE + TEXT +
                CLOSE_BRACKET;

        db.execSQL(CREATE_CITIES_TABLE);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion != newVersion) {
            db.execSQL(DROP_TABLE_IF_EXISTS + SPACE + TABLE_CITIES);
            onCreate(db);
        }
    }


    public void addCity(City city, DatabaseTransactionListener listener) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();

        try {
            ContentValues values = new ContentValues();
            values.put(KEY_LAT, city.getLatLng().latitude);
            values.put(KEY_LON, city.getLatLng().longitude);
            values.put(KEY_NAME, city.getName());

            db.insertOrThrow(TABLE_CITIES, null, values);
            db.setTransactionSuccessful();
        } catch (Exception e) {
            L.e(TAG, "Error while trying to add city to database");
            listener.onError();
        } finally {
            listener.onSuccess();
            notifyDatasetChanged();
            db.endTransaction();
        }
    }

    // Get all cities in the database
    public List<City> getAllCities() {
        List<City> cities = new ArrayList<>();

        String CITY_SELECT_QUERY = "SELECT * FROM " + TABLE_CITIES;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(CITY_SELECT_QUERY, null);
        try {
            if (cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(KEY_CITY_ID));
                    String name = cursor.getString(cursor.getColumnIndex(KEY_NAME));
                    String strLat = cursor.getString(cursor.getColumnIndex(KEY_LAT));
                    String strLon = cursor.getString(cursor.getColumnIndex(KEY_LON));

                    double lat = Double.parseDouble(strLat);
                    double lon = Double.parseDouble(strLon);

                    LatLng latLng = new LatLng(lat, lon);

                    City city = new City(latLng, name);
                    city.setDatabaseId(id);

                    cities.add(city);
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
            L.e(TAG, "Error while trying to get cities from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return cities;
    }

    public void deleteCity(int cityId) {
        SQLiteDatabase db = getWritableDatabase();
        db.beginTransaction();
        try {
            db.delete(TABLE_CITIES, KEY_CITY_ID +"=?", new String[]{Integer.toString(cityId)});
            db.setTransactionSuccessful();
        } catch (Exception e) {
            L.e(TAG, "Error while trying to delete city");
        } finally {
            db.endTransaction();
            notifyDatasetChanged();
        }
    }

    private void notifyDatasetChanged() {
        applicationContext.sendBroadcast(new Intent(DatabaseChangedReceiver.ACTION_DATABASE_CHANGED));
    }

    public City getCityById(int cityId) {

        String CITY_SELECT_QUERY = "SELECT * FROM " + TABLE_CITIES + " WHERE " + KEY_CITY_ID + " = " + cityId;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.rawQuery(CITY_SELECT_QUERY, null);

        try {
            if (cursor.moveToFirst()) {
                do {
                    int id = cursor.getInt(cursor.getColumnIndex(KEY_CITY_ID));
                    String name = cursor.getString(cursor.getColumnIndex(KEY_NAME));
                    String strLat = cursor.getString(cursor.getColumnIndex(KEY_LAT));
                    String strLon = cursor.getString(cursor.getColumnIndex(KEY_LON));

                    double lat = Double.parseDouble(strLat);
                    double lon = Double.parseDouble(strLon);

                    LatLng latLng = new LatLng(lat, lon);

                    City city = new City(latLng, name);
                    city.setDatabaseId(id);

                    return city;
                } while(cursor.moveToNext());
            }
        } catch (Exception e) {
            L.e(TAG, "Error while trying to get cities from database");
        } finally {
            if (cursor != null && !cursor.isClosed()) {
                cursor.close();
            }
        }
        return null;
    }
}
