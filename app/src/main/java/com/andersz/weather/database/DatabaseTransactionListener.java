package com.andersz.weather.database;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public interface DatabaseTransactionListener {

    void onSuccess();

    void onError();

}
