package com.andersz.weather.database;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.andersz.weather.utils.L;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class DatabaseChangedReceiver extends BroadcastReceiver {
    private static final String TAG = DatabaseChangedReceiver.class.getSimpleName();

    public static String ACTION_DATABASE_CHANGED = "com.andersz.weather.DATABASE_CHANGED";

    public static IntentFilter getIntentFiler() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_DATABASE_CHANGED);

        return filter;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        L.d(TAG, "onReceive");
    }
}