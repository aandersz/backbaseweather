package com.andersz.weather.model.api;

import java.util.List;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */


public class WeatherApi {

    public Coord coord;
    public List<Weather> weather = null;
    public String base;
    public Main main;
    public Wind wind;
    public Clouds clouds;
    public Integer dt;
    public Sys sys;
    public Integer id;
    public String name;
    public Integer cod;

}