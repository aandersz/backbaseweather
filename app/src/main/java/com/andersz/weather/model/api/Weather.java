package com.andersz.weather.model.api;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class Weather {

    public Integer id;
    public String main;
    public String description;
    public String icon;

}