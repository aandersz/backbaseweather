
package com.andersz.weather.model.api;


public class List {

    public Integer dt;
    public Main main;
    public java.util.List<Weather> weather = null;
    public Clouds clouds;
    public Wind wind;
    public Rain rain;
    public Sys sys;
    public String dtTxt;

}
