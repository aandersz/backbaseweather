package com.andersz.weather.model.api;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class Sys {

    public Double message;
    public Integer sunrise;
    public Integer sunset;

}