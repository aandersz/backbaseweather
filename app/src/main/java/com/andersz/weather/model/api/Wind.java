package com.andersz.weather.model.api;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class Wind {

    public Double speed;
    public Double deg;

}