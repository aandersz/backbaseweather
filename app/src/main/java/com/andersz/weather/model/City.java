package com.andersz.weather.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class City {

    private int databaseId;
    private LatLng latLng;
    private String name;

    public City(LatLng latLng, String name) {
        this.latLng = latLng;
        this.name = name;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDatabaseId() {
        return databaseId;
    }

    public void setDatabaseId(int databaseId) {
        this.databaseId = databaseId;
    }
}
