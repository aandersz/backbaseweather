package com.andersz.weather.model.api;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class Coord {

    public Integer lon;
    public Integer lat;

}