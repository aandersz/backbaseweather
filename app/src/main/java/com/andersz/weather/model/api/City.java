
package com.andersz.weather.model.api;


public class City {

    public Integer id;
    public String name;
    public Coord coord;
    public Integer population;

}
