package com.andersz.weather.model.api;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */


public class Main {

    public Double temp;
    public Double pressure;
    public Integer humidity;
    public Double tempMin;
    public Double tempMax;
    public Double seaLevel;
    public Double grndLevel;

}