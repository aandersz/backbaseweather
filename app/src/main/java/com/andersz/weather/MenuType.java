package com.andersz.weather;

import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.andersz.weather.screen.help.HelpFragment;
import com.andersz.weather.screen.home.HomeFragment;
import com.andersz.weather.screen.settings.SettingsFragment;

/**
 * @author Adrian Andersz
 */

public enum MenuType {

    HOME() {
        @Override
        public int getResId() {
            return R.id.navigation_action_home;
        }

        @Override
        public Fragment getFragment() {
            return HomeFragment.newInstance();
        }
    },

    HELP() {
        @Override
        public int getResId() {
            return R.id.navigation_action_help;
        }

        @Override
        public Fragment getFragment() {
            return HelpFragment.newInstance();
        }
    },

    SETTINGS() {
        @Override
        public int getResId() {
            return R.id.navigation_action_settings;
        }

        @Override
        public Fragment getFragment() {
            return SettingsFragment.newInstance();
        }
    };

    private int resId;

    private Fragment fragment;

    public int getResId() {
        return resId;
    }

    public Fragment getFragment() {
        return fragment;
    }

    public static MenuType from(MenuItem menuItem) {
        for (MenuType menuType : values()) {
            if(menuType.getResId() == menuItem.getItemId()) {
                return menuType;
            }
        }

        throw new IllegalArgumentException("Not supported menuItemId in MenuTypes" + menuItem.getItemId());
    }
}



