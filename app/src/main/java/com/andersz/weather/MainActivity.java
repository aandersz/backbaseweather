package com.andersz.weather;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.view.MenuItem;

import com.andersz.weather.main.BaseActivity;

import static com.andersz.weather.MenuType.HELP;
import static com.andersz.weather.MenuType.HOME;
import static com.andersz.weather.MenuType.SETTINGS;

public class MainActivity extends BaseActivity {

    private Fragment switchFragment;

    private BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initBottomNavigationListener();

        bottomNavigationView.setSelectedItemId(HOME.getResId());
    }

    private void initViews() {
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
    }

    private void initBottomNavigationListener() {

        bottomNavigationView.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (MenuType.from(item)) {
                            case HOME:
                                switchFragment = HOME.getFragment();
                                break;

                            case HELP:
                                switchFragment = HELP.getFragment();
                                break;

                            case SETTINGS:
                                switchFragment = SETTINGS.getFragment();
                                break;

                            default:
                                throw new IllegalStateException("Unhandled navigation item");
                        }

                        if(switchFragment != null) {
                            replaceFragment(switchFragment, true);
                        }

                        return true;
                    }
                });
    }
}
