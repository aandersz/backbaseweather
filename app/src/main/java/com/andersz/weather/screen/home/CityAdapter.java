package com.andersz.weather.screen.home;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.andersz.weather.R;
import com.andersz.weather.model.City;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {

    private List<City> cities = new ArrayList<>();

    private HomePresenter presenter;

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textView;

        public ViewHolder(View v) {
            super(v);
            textView = (TextView) v.findViewById(R.id.item_city_name);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    City city = getItem(getAdapterPosition());
                    if(presenter != null) {
                        presenter.itemOnClick(city);
                    }
                }
            });

            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    City city = getItem(getAdapterPosition());
                    if(presenter != null) {
                        presenter.itemOnLongPress(city);
                    }
                    return true;
                }
            });
        }
    }

    public CityAdapter() {
    }

    public CityAdapter(List<City> cities) {
        this.cities = cities;
    }

    public List<City> getCities() {
        return cities;
    }

    public void setCities(List<City> cities) {
        this.cities = cities;
        notifyDataSetChanged();
    }

    public void setPresenter(HomePresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public CityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_city_cardview, parent, false);

        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        City city = cities.get(position);
        holder.textView.setText(city.getName());
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    private City getItem(int position) {
        return cities.get(position);
    }
}
