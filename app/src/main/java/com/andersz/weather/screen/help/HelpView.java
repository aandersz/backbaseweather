package com.andersz.weather.screen.help;

import android.webkit.WebView;

/**
 * @author Adrian Andersz
 */

public interface HelpView {
    WebView getWebView();
}
