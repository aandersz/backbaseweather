package com.andersz.weather.screen.help;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import com.andersz.weather.C;
import com.andersz.weather.R;
import com.andersz.weather.main.BaseFragment;

/**
 * @author Adrian Andersz
 */

public class HelpFragment extends BaseFragment implements HelpView {

    public static HelpFragment newInstance () {
        return new HelpFragment();
    }

    WebView webView;

    HelpPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new HelpPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_help, container, false);
        bindViews(v);
        initPresenter();

        webView.loadUrl(C.Url.HELP_SCREEN);

        return v;
    }

    private void bindViews(View v) {
        webView = (WebView) v.findViewById(R.id.help_webview);
    }

    private void initPresenter() {
        presenter = new HelpPresenter();
        presenter.attachView(this);
        presenter.onStart();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    @Override
    public WebView getWebView() {
        return webView;
    }
}
