package com.andersz.weather.screen.settings;

import com.andersz.weather.main.AbstractPresenter;

/**
 * Presenter for handling Home Screen business logic
 *
 * @author Adrian Andersz
 */

public class SettingsPresenter extends AbstractPresenter<SettingsView>{


    @Override
    protected void onStart() {

    }
}
