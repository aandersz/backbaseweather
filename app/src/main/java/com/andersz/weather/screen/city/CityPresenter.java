package com.andersz.weather.screen.city;

import android.content.Context;
import android.view.View;

import com.andersz.weather.database.DatabaseHelper;
import com.andersz.weather.main.AbstractPresenter;
import com.andersz.weather.model.City;
import com.andersz.weather.network.Forecast;
import com.andersz.weather.network.NetworkResponseListener;
import com.andersz.weather.network.QueryFactory;
import com.andersz.weather.network.TodayWeather;
import com.andersz.weather.utils.L;

import static com.andersz.weather.C.Units.METRIC;

/**
 * Presenter for handling Home Screen business logic
 *
 * @author Adrian Andersz
 */

public class CityPresenter extends AbstractPresenter<CityView>{
    private static final String TAG = CityPresenter.class.getSimpleName();

    private Context context;
    private int cityId;

    @Override
    protected void onStart() {
        loadData();
    }

    private void loadData(){
        QueryFactory queryFactory = new QueryFactory();
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(this.context);
        City city = databaseHelper.getCityById(cityId);

        new TodayWeather(new NetworkResponseListener() {
            @Override
            public void onSuccess(String result) {
                L.d(TAG, result);
                view.getWeatherTextView().setText(result);
                view.getProgressBar().setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError() {
                view.getProgressBar().setVisibility(View.INVISIBLE);
                view.showError();
            }
        }).execute(queryFactory.getWeatherQuery(city.getLatLng(), METRIC));

        new Forecast(new NetworkResponseListener() {
            @Override
            public void onSuccess(String result) {
                L.d(TAG, result);
                view.getForecastTextView().setText(result);
                view.getProgressBar().setVisibility(View.INVISIBLE);
            }

            @Override
            public void onError() {
                view.getProgressBar().setVisibility(View.INVISIBLE);
                view.showError();
            }
        }).execute(queryFactory.getForecastQuery(city.getLatLng(), METRIC));

    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
