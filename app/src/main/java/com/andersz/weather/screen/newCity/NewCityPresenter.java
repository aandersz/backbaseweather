package com.andersz.weather.screen.newCity;

import android.content.Context;

import com.andersz.weather.C;
import com.andersz.weather.database.DatabaseHelper;
import com.andersz.weather.database.DatabaseTransactionListener;
import com.andersz.weather.main.AbstractPresenter;
import com.andersz.weather.model.City;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class NewCityPresenter extends AbstractPresenter<NewCityView> implements DatabaseTransactionListener {

    private Context context;

    private MarkerOptions markerOptions;

    private Marker marker;

    @Override
    protected void onStart() {

    }

    protected void setContext(Context context) {
        this.context = context;
    }

    protected void addMarker(GoogleMap googleMap, LatLng latLng) {
        if(marker != null) {
            marker.remove();
        }
        markerOptions = new MarkerOptions()
                .position(latLng);

        marker = googleMap.addMarker(markerOptions);
    }

    protected void createNewCity() {
        if(marker == null || markerOptions == null) {
            view.showSnackbar(C.ErrorType.NO_MARKER);
            return;
        }

        if (view.getCityName() == null || view.getCityName().getText() == null || view.getCityName().getText().toString().isEmpty()) {
            view.showSnackbar(C.ErrorType.NO_NAME);
            return;
        }

        String name = view.getCityName().getText().toString();

        City city = new City(markerOptions.getPosition(), name);
        saveToDatabase(city);
    }

    private void saveToDatabase(City city) {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(this.context);
        databaseHelper.addCity(city, this);
    }

    @Override
    public void onSuccess() {
        view.finishActivity();
    }

    @Override
    public void onError() {
        view.showSnackbar(C.ErrorType.GENERIC);
    }
}
