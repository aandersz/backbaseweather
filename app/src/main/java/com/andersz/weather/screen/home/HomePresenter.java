package com.andersz.weather.screen.home;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;

import com.andersz.weather.database.DatabaseChangedReceiver;
import com.andersz.weather.database.DatabaseHelper;
import com.andersz.weather.main.AbstractPresenter;
import com.andersz.weather.model.City;
import com.andersz.weather.screen.city.CityActivity;

import java.util.List;

import static com.andersz.weather.database.DatabaseChangedReceiver.ACTION_DATABASE_CHANGED;

/**
 * Presenter for handling Home Screen business logic
 *
 * @author Adrian Andersz
 */

public class HomePresenter extends AbstractPresenter<HomeView> {

    private DatabaseChangedReceiver receiver;

    private Context context;

    @Override
    protected void onStart() {
        registerDatabaseReceiver();
        bindPresenterToAdapter();
        loadData();
    }

    //// FIXME: 09/07/17 looks a bit nasty. Move more recycle view build to presenter
    private void bindPresenterToAdapter() {
        ((CityAdapter)view.getAdapter()).setPresenter(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver();
    }

    public void loadData() {
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(this.context);
        List<City> cities = databaseHelper.getAllCities();
        ((CityAdapter) view.getAdapter()).setCities(cities);
    }

    public void setContext(Context context) {
        this.context = context;
    }

    private void registerDatabaseReceiver() {
        receiver = new DatabaseChangedReceiver() {
            public void onReceive(Context context, Intent intent) {
                loadData();
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(ACTION_DATABASE_CHANGED);

        context.registerReceiver(receiver, filter);
    }

    public void unregisterReceiver() {
        if (receiver != null) {
            context.unregisterReceiver(receiver);
            receiver = null;
        }
    }

    private void openCityWeather(int cityId) {
        context.startActivity(CityActivity.createIntent(this.context, cityId));
    }

    public void itemOnClick(City city) {
        openCityWeather(city.getDatabaseId());
    }

    public void itemOnLongPress(City city) {
        //todo - create a dialog with confirmation or something
        DatabaseHelper databaseHelper = DatabaseHelper.getInstance(this.context);
        databaseHelper.deleteCity(city.getDatabaseId());
    }
}
