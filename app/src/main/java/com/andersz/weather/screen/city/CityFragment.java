package com.andersz.weather.screen.city;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.andersz.weather.R;
import com.andersz.weather.main.BaseActivity;
import com.andersz.weather.main.BaseFragment;

import static com.andersz.weather.C.BundleKey.KEY_CITY_ID;

/**
 * @author Adrian Andersz
 */

public class CityFragment extends BaseFragment implements CityView {

    public static CityFragment newInstance (int cityId) {
        CityFragment cityFragment = new CityFragment();
        setArguments(cityFragment, cityId);
        return cityFragment;
    }

    protected static void setArguments(CityFragment cityFragment, int cityId) {
        Bundle bundle = new Bundle();
        bundle.putInt(KEY_CITY_ID, cityId);
        cityFragment.setArguments(bundle);
    }

    private TextView weatherView;
    private TextView forecastView;
    private ProgressBar progressBar;

    protected CityPresenter presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new CityPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_city, container, false);

        initPresenter();
        bindViews(v);

        return v;
    }

    private void bindViews(View v) {
        weatherView = (TextView) v.findViewById(R.id.city_weather);
        forecastView = (TextView) v.findViewById(R.id.city_forecast);
        progressBar = (ProgressBar) v.findViewById(R.id.city_progress_view);
     }

    private void initPresenter() {
        int cityId = getArguments().getInt(KEY_CITY_ID);

        presenter = new CityPresenter();
        presenter.setContext(this.getContext());
        presenter.setCityId(cityId);
        presenter.attachView(this);
        presenter.onStart();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        presenter.detachView();
    }

    @Override
    public TextView getWeatherTextView() {
        return weatherView;
    }

    @Override
    public TextView getForecastTextView() {
        return forecastView;
    }

    @Override
    public ProgressBar getProgressBar() {
        return progressBar;
    }

    @Override
    public void showError() {
        ((BaseActivity) getActivity()).showSnackbar(getString(R.string.main_generic_error));
    }


}
