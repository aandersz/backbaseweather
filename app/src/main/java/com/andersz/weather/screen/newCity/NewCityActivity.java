package com.andersz.weather.screen.newCity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;

import com.andersz.weather.C;
import com.andersz.weather.R;
import com.andersz.weather.main.BaseActivity;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;

/**
 * @author Adrian Andersz
 */

public class NewCityActivity extends BaseActivity implements NewCityView, OnMapReadyCallback {

    public static Intent createIntent(final Context context) {
        Intent intent = new Intent(context, NewCityActivity.class);
        return intent;
    }

    private NewCityPresenter presenter;

    private EditText cityName;

    private SupportMapFragment mapFragment;

    private GoogleMap googleMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_city);

        initPresenter();
        bindViews();
        initMapFragment();
    }

    private void initPresenter() {
        presenter = new NewCityPresenter();
        presenter.attachView(this);
        presenter.setContext(this);
    }

    private void bindViews() {
        cityName = (EditText) findViewById(R.id.new_city_name);
    }

    private void initMapFragment() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        initGoogleMap(googleMap);
    }

    private void initGoogleMap(final GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {
            @Override
            public void onMapLongClick(LatLng latLng) {
                presenter.addMarker(NewCityActivity.this.googleMap, latLng);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_newcity_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_done:
                presenter.createNewCity();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public EditText getCityName() {
        return cityName;
    }

    @Override
    public void showSnackbar(String message) {
        String snackbarMessage;

        switch (message){
            case C.ErrorType.NO_MARKER:
                snackbarMessage = getString(R.string.new_city_error_no_marker);
                break;
            case C.ErrorType.NO_NAME:
                snackbarMessage = getString(R.string.new_city_error_no_name);
                break;

            case C.ErrorType.GENERIC:
                snackbarMessage = getString(R.string.main_generic_error);
                break;

            default:
                throw new IllegalArgumentException("invalid message type");
        }

        super.showSnackbar(snackbarMessage);
    }

    @Override
    public void finishActivity() {
        this.finish();
    }
}
