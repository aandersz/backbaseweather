package com.andersz.weather.screen.home;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.andersz.weather.R;
import com.andersz.weather.main.BaseFragment;
import com.andersz.weather.screen.newCity.NewCityActivity;

/**
 * @author Adrian Andersz
 */

public class HomeFragment extends BaseFragment implements HomeView {

    public static HomeFragment newInstance () {
        return new HomeFragment();
    }

    private HomePresenter presenter;

    private FloatingActionButton fabAdd;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void initFragment(Bundle savedInstanceState) {
        super.initFragment(savedInstanceState);
        initPresenter();
    }

    @Nullable
    @Override
    public View onCreateView(final LayoutInflater inflater, @Nullable final ViewGroup container, @Nullable final Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        bindViews(view);
        initListeners();

        return view;
    }

    private void initPresenter() {
        presenter = new HomePresenter();
        presenter.setContext(this.getContext());
        presenter.attachView(this);
        presenter.onStart();
    }

    private void bindViews(View view) {
        fabAdd = (FloatingActionButton) view.findViewById(R.id.home_fab_add);

        recyclerView = (RecyclerView) view.findViewById(R.id.home_recycler_view);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this.getContext());
        recyclerView.setLayoutManager(layoutManager);

        adapter = new CityAdapter();
        recyclerView.setAdapter(adapter);
    }

    private void initListeners() {
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = getContext();
                context.startActivity(NewCityActivity.createIntent(context));
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.detachView();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.unregisterReceiver();
    }

    @Override
    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }
}
