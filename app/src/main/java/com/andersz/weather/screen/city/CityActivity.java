package com.andersz.weather.screen.city;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.andersz.weather.R;
import com.andersz.weather.main.BaseActivity;

import static com.andersz.weather.C.BundleKey.KEY_CITY_ID;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

public class CityActivity extends BaseActivity {
    private static final String TAG = CityActivity.class.getSimpleName();

    public static Intent createIntent(final Context context, int cityId) {
        Intent intent = new Intent(context, CityActivity.class);
        intent.putExtra(KEY_CITY_ID, cityId);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_city);

        int cityId = getIntent().getIntExtra(KEY_CITY_ID, 0);

        replaceFragment(CityFragment.newInstance(cityId), false);
    }
}
