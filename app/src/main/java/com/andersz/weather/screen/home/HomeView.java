package com.andersz.weather.screen.home;

import android.support.v7.widget.RecyclerView;

/**
 * @author Adrian Andersz
 */

public interface HomeView {
    RecyclerView.Adapter getAdapter();
}
