package com.andersz.weather.screen.help;

import com.andersz.weather.C;
import com.andersz.weather.main.AbstractPresenter;

/**
 * Presenter for handling help Screen business logic
 *
 * @author Adrian Andersz
 */

public class HelpPresenter extends AbstractPresenter<HelpView>{

    @Override
    protected void onStart() {
        loadPage();
    }

    public void loadPage() {
        view.getWebView().loadUrl(C.Url.HELP_SCREEN);
    }
}
