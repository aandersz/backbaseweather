package com.andersz.weather.screen.newCity;

import android.widget.EditText;

/**
 * @author Adrian Andersz <adrian.andersz@gmail.com>
 */

interface NewCityView {

    EditText getCityName();

    void showSnackbar(String message);

    void finishActivity();
}
