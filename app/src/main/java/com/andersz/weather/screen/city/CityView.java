package com.andersz.weather.screen.city;

import android.widget.ProgressBar;
import android.widget.TextView;

/**
 * @author Adrian Andersz
 */

public interface CityView {

    TextView getWeatherTextView();

    TextView getForecastTextView();

    ProgressBar getProgressBar();

    void showError();
}
